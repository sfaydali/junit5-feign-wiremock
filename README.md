# Junit5 Wiremock Extension for Feign Client Test

[![license](https://img.shields.io/hexpm/l/plug)](https://gitlab.com/sfaydali/junit5-feign-wiremock/-/raw/master/LICENSE)

The junit5-feign-wiremock package provides a simple and easy to use annotation to test [spring cloud open feign clients](https://spring.io/projects/spring-cloud-openfeign) via [wiremock](http://wiremock.org/).

This package consist of a single annotation(`WireMockTest`) that runs the wiremock server on the given port in localhost. It runs the server before the tests run and resets the stubs after each test. When all tests are completed, it shuts down the server.

## Installation

First, you need to add the GitLab maven repo.

```
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/packages/maven</url>
    </repository>
</repositories>
```

The package can be used by adding dependencies.

```
<dependency>
    <groupId>com.sfydli</groupId>
    <artifactId>junit5-feign-wiremock</artifactId>
    <version>1.0</version>
    <scope>test</scope>
</dependency>
```

## Example Usage

First, let's create our sample client scenario.

>main/resources/application.properties
```properties
sample-api=https://sample-api-url.com
```

>SampleClient.java

```java
@FeignClient(value = "sample-api", url = "${sample-api.url}")
public interface SampleApiClient {

    @GetMapping("/sample-endpoint")
    SampleResponse getSample();
}
```

Now we can write the client test.

>test/resources/application.properties
```properties
sample-api=localhost:9599
```

>SampleClientTest.java

```java
@WireMockTest(port = 9599)
@SpringBootTest(classes = {SampleApiClient.class, ObjectMapper.class})
class SampleApiClientTest {

    @Autowired
    private SampleApiClient sampleApiClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void it_should_get_sample() throws JsonProcessingException {
        // given
        final var expected = new SampleResponse();

        stubFor(get(urlEqualTo("/sample-endpoint"))
                .willReturn(okJson(objectMapper.writeValueAsString(expected)))
        );

        // when
        final var actual = sampleApiClient.getSample();

        // then
        assertThat(actual).isEqualToComparingFieldByField(expected);
    }
}
```